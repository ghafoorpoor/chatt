package GUI;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenoStart {
    JFrame f=new JFrame();

    void start(){
        JLabel textArea=new JLabel("سیستم آزمون یار ");
        textArea.setBounds(550, 40, 200, 200);
        textArea.setBackground(Color.blue);
        JButton b1=new JButton("پنل مدیریت ");
        b1.setBounds(100, 300, 200, 50);
        JButton b2=new JButton("پنل کاربری ");
        b2.setBounds(1000, 300, 200, 50);

        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PanelKarbar panelKarbar= new PanelKarbar();
                panelKarbar.Start("student.csv");
               f.setVisible(false);
            }
        });
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PanelModirait panelModirait=new PanelModirait();
                panelModirait.Start("modiriat.csv");
                f.setVisible(false);
            }
        });
        f.setSize(1400, 1000);
        f.setBackground(Color.gray);
        f.add(b1);
        f.add(b2);
        f.add(textArea);
        f.setLayout(null);
        f.setVisible(true);

    }
}
