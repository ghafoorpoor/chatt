package GUI;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Enter {
    String[] line = new String[10];

    boolean enter(String path, String name, String pass) {
        BufferedReader read = null;
        try {
            read = new BufferedReader(new FileReader(path));
            while (true) {
                String li = read.readLine();
                if (li != null) {
                    line=li.split(",");
                    if(line[3].equals(name))
                        if(line[4].equals(pass)){
                            System.out.println(line[3]+"   "+line[4]);
                            return true;
                        }
                }
                if (li == null) {
                    break;
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
