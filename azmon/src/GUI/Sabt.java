package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.ArrayList;
import java.util.jar.JarEntry;

public class Sabt {
    JFrame f=new JFrame();
    String nam,fami,pas,karbar;
    long idd;
    int con=0;
    String line2;
    boolean check=true;
    String[]karba=new String[10];

    JLabel  l1,l2,l3,l4,l5;
    void start(String paht) {
        JButton b1 = new JButton("ثبت");
        b1.setBounds(600, 500, 60, 40);
        l1 = new JLabel("نام");
        l1.setBounds(800, 60, 50, 30);
        JTextField name = new JTextField();
        name.setBounds(550, 60, 200, 50);
        l2 = new JLabel("فامیل");
        l2.setBounds(800, 130, 50, 30);
        JTextField family = new JTextField();
        family.setBounds(550, 130, 200, 50);
        l3 = new JLabel("شماره دانشجویی");
        l3.setBounds(800, 200, 80, 30);
        JTextField id = new JTextField();
        id.setBounds(550, 200, 200, 50);
        l4 = new JLabel("کاربری");
        l4.setBounds(800, 270, 50, 30);
        JTextField karbary = new JTextField();
        karbary.setBounds(550, 270, 200, 50);
        l5 = new JLabel("رمز");
        l5.setBounds(800, 340, 50, 30);
        JTextField pass = new JTextField();
        pass.setBounds(550, 340, 200, 50);
        f.setSize(1400, 1000);
        f.setBackground(Color.gray);

        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {//وقتی برنامه بسته میشه و دوراه ران میشه دخیره های قبلی پاک میشه
                nam = name.getText();
                fami = family.getText();
                long idd = Integer.parseInt(id.getText());
                karbar = karbary.getText();
                pas = pass.getText();
                ArrayList<String> line = new ArrayList<String>();
                BufferedReader read=null;
                try {
                    read=new BufferedReader(new FileReader(paht));
                    while (true) {
                        String li=read.readLine();
                        if(li!=null) {
                            line.add(li);
                            //System.out.println(li);
                            karba=li.split(",");
                            if(karba[3].equals(karbar)){
                                check=false;
                            }
                            con++;
                        }
                        if (li == null) {
                            break;
                        }
                    }
                    if(!check){
                        JOptionPane.showMessageDialog(f,"Enter the username again.");
                        check=true;

                    }else {
                        con = line.size();
                        con++;
                        line2 = nam + ',' + fami + ',' + idd + ',' + karbar + ',' + pas;
                        line.add(line2);
                    }
                            try (PrintWriter writer = new PrintWriter(new File(paht))) {
                                StringBuilder sb = new StringBuilder();

                                for (int i=0;i<con;i++){
                                    sb.append(line.get(i));
                                    sb.append('\n');
                                }

                                writer.write(sb.toString());

                                System.out.println("done!");

                            } catch (FileNotFoundException v) {
                                System.out.println(v.getMessage());
                            }

                } catch (FileNotFoundException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                finally {
                    try {
                        read.close();
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }
                }
            }
        });
        f.add(name);
        f.add(family);
        f.add(id);
        f.add(karbary);
        f.add(pass);
        f.add(b1);
        f.add(l1);
        f.add(l2);
        f.add(l3);
        f.add(l4);
        f.add(l5);
        f.setLayout(null);
        f.setVisible(true);

      
    }
    }
