package az_far.src.Logic;

import java.io.*;

public class Entrance {

    private String mode;
    private String kind_of_person;
    private String firstname, lastname;
    private String user, password;
    private int id;
    private boolean login() throws IOException {
        boolean valid=false;
        if(getKind_of_person().equals("student")){
            // System.out.println("getKind_of_person() is valid!!");
            File file = new File("C:\\Users\\LP Z R\\IdeaProjects\\azmon\\src\\az_far\\student.csv");
            BufferedReader reader1 = null;
            try {
                reader1=new BufferedReader(new FileReader(file));
                String read;
                while ((read=reader1.readLine())!=null){
                    String[] split=read.split(",");
                    if(split[2].contains(String.valueOf(user)) && split[3].contains(String.valueOf(password))){
                        valid=true; break;
                    }
                }
            }finally {
                reader1.close();
            }
        }
        else{
            File file = new File("C:\\Users\\LP Z R\\IdeaProjects\\azmon\\src\\az_far\\teacher.csv");
            BufferedReader reader2 = null;
            try {
                reader2=new BufferedReader(new FileReader(file));
                String read;
                while ((read=reader2.readLine())!=null){
                    String[] split=read.split(",");
                    if(split[2].equals(getUser()) && split[3].equals(getPassword())){
                        valid=true; break;
                    }
                }
            }finally {
                reader2.close();
            }
        }
        return valid;
    }
    private boolean register() throws IOException {
        boolean valid=true;
        if(getKind_of_person().equals("Student")){
            File file = new File("C:\\Users\\LP Z R\\IdeaProjects\\azmon\\src\\az_far\\student.csv");

            BufferedReader reader = null;
            try {
                reader=new BufferedReader(new FileReader(file));

                StringBuffer info=new StringBuffer();
                String line;
                while ((line=reader.readLine())!=null){
                    info.append(line+"\n");
                }
                System.out.println("saved info: "+info);
                BufferedReader reader1 = null;
                try {
                    reader1=new BufferedReader(new FileReader(file));
                    String read;
                    while ((read=reader1.readLine())!=null){
                        String[] split=read.split(",");
                        if((split[0].equals(getFirstname()) && split[1].equals(getLastname())) || split[4].equals(String.valueOf(getId())) || split[2].equals(getUser()) || split[3].equals(getPassword())){
                            valid=false; break;
                        }
                    }
                }finally {
                    reader1.close();
                }
                System.out.println("valid enter:"+valid);

                if(valid){
                    PrintWriter writer=null;
                    try {
                        info.append(firstname+","+lastname+","+user+","+password+","+(id)+"\n");
                        //student sru=new student(id,user,password,firstname,lastname);
                        writer=new PrintWriter(new BufferedWriter(new FileWriter(file)));
                        writer.print(info);
                        System.out.println("completed!!");
                    }finally {
                        writer.close();
                    }

                }else System.out.println("these informations has registered!!!!");
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                reader.close();
            }
        }else{
            File file = new File("C:\\Users\\LP Z R\\IdeaProjects\\azmon\\src\\az_far\\teacher.csv");
            BufferedReader reader = null;
            try {
                reader=new BufferedReader(new FileReader(file));

                StringBuffer info=new StringBuffer();
                String line;
                while ((line=reader.readLine())!=null){
                    info.append(line+"\n");
                }

                System.out.println("saved info: "+info);
                BufferedReader reader1 = null;
                try {
                    reader1=new BufferedReader(new FileReader(file));
                    String read=null;
                    while ((read=reader1.readLine())!=null){
                        String[] split=read.split(",");
                        if((split[0].equals(getFirstname()) && split[1].equals(getLastname()))  || split[2].equals(getUser()) || split[3].equals(getPassword())){
                            valid=false; break;
                        }
                    }
                }finally {
                    reader1.close();
                }
                System.out.println("valid:"+valid);
                if(valid){
                    PrintWriter writer=null;
                    try {
                        info.append(firstname+","+lastname+","+user+","+password+"\n");
                        //student sru=new student(id,user,password,firstname,lastname);
                        writer=new PrintWriter(new BufferedWriter(new FileWriter(file)));
                        writer.print(info);
                        System.out.println("completed!!");
                    }finally {
                        writer.close();
                    }

                }else System.out.println("these informations has registered!!!!");
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                reader.close();
            }
        }
        return valid;
    }
    public boolean csv() throws IOException {
        if (mode.matches("login|Login")) {
            if (kind_of_person.matches("student|Student")) {
                //System.out.println("i am in login mode and student!!");
                return login();
            } else if (kind_of_person.matches("teacher|Teacher")) {
                return login();
            } else return false;

        } else if (mode.matches("Register|register")) {

            if (kind_of_person.matches("student|Student")) {
                //System.out.println("student register reached!!");
                //return true;
                return register();
            } else if (kind_of_person.matches("teacher|Teacher")) {
                return register();
            } else return false;


        }else return false;
    }


    public String getMode () {
        return mode;
    }

    public void setMode (String mode){
        this.mode = mode;
    }

    public String getFirstname () {
        return firstname;
    }

    public void setFirstname (String firstname){
        this.firstname = firstname;
    }

    public String getLastname () {
        return lastname;
    }

    public void setLastname (String lastname){
        this.lastname = lastname;
    }

    public int getId () {
        return id;
    }

    public void setId ( int id){
        this.id = id;
    }

    public String getUser () {
        return user;
    }

    public void setUser ( String user){
        this.user = user;
    }

    public String getPassword () {
        return password;
    }

    public void setPassword ( String password){
        this.password = password;
    }

    public String getKind_of_person() {
        return kind_of_person;
    }

    public void setKind_of_person(String kind_of_person) {
        this.kind_of_person = kind_of_person;
    }

    public Entrance(String mode, String kind_of_person) {
        setMode(mode);
        setKind_of_person(kind_of_person);
    }

}
