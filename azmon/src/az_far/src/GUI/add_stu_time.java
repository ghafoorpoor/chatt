package az_far.src.GUI;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTimePicker;
import com.sun.scenario.effect.impl.prism.PrReflectionPeer;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class add_stu_time implements Initializable {
public String gettime(){
    return name;
}
    @FXML
    ListView<String> stu_list1;
    @FXML
    ListView<String> stu_list2;

    @FXML
    JFXTimePicker timerPicker;
    @FXML
    JFXButton btn;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file=new File("student.csv");
        BufferedReader reader=null;
        try {
            reader=new BufferedReader(new FileReader(file));
            String line=reader.readLine();
            while((line=reader.readLine())!=null){
                stu_list1.getItems().add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        stu_list1.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

StringBuffer stringBuffer=new StringBuffer();
    public void add_stu(ActionEvent event) {

        ObservableList<String> list=stu_list1.getSelectionModel().getSelectedItems();
        int i=0;
        while (list.get(i)!=null){
            String[] str=list.get(i).split(",");
            stu_list2.getItems().add(str[4]);
            stringBuffer.append(str[4]+"\n");
            i++;
        }

    }
    String name;
    public void send(ActionEvent event){

         name= String.valueOf(timerPicker.getValue());
        name=name.replace(':','-');
        File file=new File("src\\Logic\\exams\\perticipate_stu\\"+name+".csv");

        PrintWriter writer=null;
        try {
            writer=new PrintWriter(new BufferedWriter(new FileWriter(file)));


                writer.print(stringBuffer);

            System.out.println("Succced!!");
        }catch (IOException e){
            e.printStackTrace();
        }finally {
            writer.close();
        }


    }
}
