package az_far.src.GUI;

import az_far.src.Logic.teacher;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerBackArrowBasicTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class teacher_UI implements Initializable {
    private teacher teacher;

    //teacher gui:
    @FXML
     JFXDrawer ui_drawer;
    @FXML
    AnchorPane first_anchor;
    @FXML
    JFXHamburger ui_ham;
    @FXML
    HBox hbox;
    //**********
    //controller_exam_bar:
    @FXML
     JFXDrawer exam_drawer;
    @FXML VBox side_vbox;
    @FXML JFXHamburger side_exam_ham;

//add_stu:


    public void set(teacher teacher){
        this.teacher=teacher;
    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        ui_drawer.setSidePane(hbox);
        HamburgerBackArrowBasicTransition ui_d=new HamburgerBackArrowBasicTransition(ui_ham);
        ui_d.setRate(-1);
        ui_ham.addEventHandler(MouseEvent.MOUSE_PRESSED,(e)->{
            ui_d.setRate(ui_d.getRate()*(-1));
            ui_d.play();
            if(ui_drawer.isOpened()){
                ui_drawer.close();
            }else{
                ui_drawer.open();
            }
        });
        exam_drawer.setSidePane(side_vbox);
        HamburgerBackArrowBasicTransition ui_exam_ham=new HamburgerBackArrowBasicTransition(ui_ham);
        ui_exam_ham.setRate(-1);
        side_exam_ham.addEventHandler(MouseEvent.MOUSE_PRESSED,(e)->{
            ui_exam_ham.setRate(ui_exam_ham.getRate()*(-2));
            ui_exam_ham.play();
            if(exam_drawer.isOpened()){
                exam_drawer.close();
            }else{
                exam_drawer.open();
            }
        });



        }

    public void add(ActionEvent event) {

        if(exam_drawer.isClosed()){
            exam_drawer.open();
            if(ui_drawer.isOpened()){
                ui_drawer.close();
            }
        }else{

        }
    }

    String timename = "";
    public void set_stu(ActionEvent event){
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("add_stu_time.fxml"));
        Parent fxml = null;
                    try {
                        loader.load();
                 fxml = FXMLLoader.load(getClass().getResource("add_stu_time.fxml"));
                //first_anchor.getChildren().removeAll();
                //first_anchor.getChildren().setAll(fxml,exam_drawer);
            } catch (IOException ex) {
               Logger.getLogger(add_stu_time.class.getName()).log(Level.SEVERE,null,ex);
                        ex.printStackTrace();
            }
                    add_stu_time time=loader.getController();
                   // AnchorPane fxml=loader.getRoot();
                    first_anchor.getChildren().removeAll();
                    first_anchor.getChildren().setAll(fxml,exam_drawer);
                    System.out.println("time.gettime:"+time.gettime());
                    //timename=time.gettime();
            }

    @FXML
    void setquestion(ActionEvent event) {
        FXMLLoader loader=new FXMLLoader();
        loader.setLocation(getClass().getResource("questions_gui.fxml"));
        try {
            loader.load();
        }catch (IOException e){
            Logger.getLogger(teacher_UI.class.getName()).log(Level.SEVERE,null,e);
        }
        QuestionsGui ques=loader.getController();
        AnchorPane fxml=loader.getRoot();
        first_anchor.getChildren().removeAll();
        first_anchor.getChildren().setAll(fxml,exam_drawer);
        ques.settime(timename);
    }


}

