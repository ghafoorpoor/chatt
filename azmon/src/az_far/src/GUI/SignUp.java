package az_far.src.GUI;

import az_far.src.Logic.Entrance;
import az_far.src.Logic.teacher;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SignUp implements Initializable {

    @FXML
    TextField user;
    @FXML
    Button register;
    @FXML
    ComboBox<String> select;
    @FXML TextField pass;
    @FXML
    VBox vBox;
    @FXML
    Label top_content;
    @FXML TextField firstname;
    @FXML TextField lastname;
    @FXML TextField ID;


    ObservableList<String> list= FXCollections.observableArrayList("Student","Teacher");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        select.setItems(list);
    }
public void setonRegistering(ActionEvent event){
        if(select.getValue().equals("Student")){
            Entrance entrance=new Entrance("register",select.getValue());
            entrance.setUser(user.getText());
            entrance.setPassword(pass.getText());
            entrance.setFirstname(firstname.getText());
            entrance.setLastname(lastname.getText());
            entrance.setId(Integer.parseInt(ID.getText()));
            System.out.println("user:"+entrance.getUser()+"\n"+"pass:"+entrance.getPassword());
            try {
                if(entrance.csv()){
                    top_content.setTextFill(Color.GREEN);
                    top_content.setText("Sign Up succeed!!");
                }else{
                    top_content.setText("Sign Up failed!!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            //vBox.getChildren().remove(ID);
            Entrance entrance=new Entrance("register",select.getValue());
            entrance.setUser(user.getText());
            entrance.setPassword(pass.getText());
            entrance.setFirstname(firstname.getText());
            entrance.setLastname(lastname.getText());
            System.out.println("user:"+entrance.getUser()+"\n"+"pass:"+entrance.getPassword());
            try {
                if(entrance.csv()){
                    top_content.setTextFill(Color.GREEN);
                    top_content.setText("Sign Up succeed!!");
                    ///////////////////////////////////////////////////////////////////////
                    teacher t=new teacher(entrance.getUser(),entrance.getPassword());
                    FXMLLoader loader=new FXMLLoader();
                    loader.setLocation(getClass().getResource("teacher_UI.fxml"));
                    try {
                        loader.load();
                    }catch (IOException e){
                        Logger.getLogger(SignUp.class.getName()).log(Level.SEVERE,null,e);
                    }
                    teacher_UI teacherUI=loader.getController();
                    Parent root=loader.getRoot();
                    Stage stage=new Stage();
                    stage.setScene(new Scene(root));
                    stage.showAndWait();
//

                }else{
                    top_content.setTextFill(Color.RED);
                    top_content.setText("Sign Up failed!!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                //vBox.getChildren().add(id);
            }
        }
}
}
