package az_far.src.GUI;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextArea;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;

public class QuestionsGui implements Initializable {

    @FXML
    private TextField choice_1;

    @FXML
    private TextField choice2;

    @FXML
    private TextField choice3;

    @FXML
    private TextField choice4;

    @FXML
    private JFXButton next;

    @FXML
    private JFXTextArea textArea_filed;

    @FXML
    private JFXButton enought;

    @FXML
    private CheckBox check1;

    @FXML
    private CheckBox check2;


    @FXML
    private CheckBox check3;

    @FXML
    private CheckBox check4;
    String time = "";
    public void settime(String str){
        time=str;
    }
    static int counter=0;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if(!choice_1.getText().isEmpty()){
            check1.setSelected(true);
        }if(!choice2.getText().isEmpty()){
            check2.setSelected(true);
        }
        if(!choice3.getText().isEmpty()){
            check3.setSelected(true);
        }
        if(!choice4.getText().isEmpty()){
            check4.setSelected(true);
        }
    }

    public void setNext(ActionEvent event){
        StringBuffer stringBuffer=new StringBuffer();
        stringBuffer.append(textArea_filed.getText());
        stringBuffer.append("\n");
        if(choice_1.getText()!=null){
            stringBuffer.append(choice_1.getText()+" ");
        }if(choice2.getText()!=null){
            stringBuffer.append(choice2.getText()+" ");
        }if(choice3.getText()!=null){
            stringBuffer.append(choice3.getText()+" ");
        }if(choice4.getText()!=null){
            stringBuffer.append(choice4.getText()+" ");
        }

        textArea_filed.clear();
        stringBuffer=null;
    }

    public void setEnought(ActionEvent event){
     textArea_filed.setDisable(true);
    }
}
