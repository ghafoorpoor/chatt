package az_far.src.GUI;

import az_far.src.Logic.teacher;
import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class teacher_gui implements Initializable {

teacher teacher;
    @FXML
    AnchorPane mainview;
    @FXML AnchorPane sidebar;
@FXML
    Button profile;
@FXML
Label label;
@FXML AnchorPane changable_layout;
public void set(teacher teacher){
    this.teacher=teacher;

}
 Parent fxml;

   @Override
    public void initialize(URL location, ResourceBundle resources) {}
    @FXML
    void MovementAdd(ActionEvent event) {

        TranslateTransition transition=new TranslateTransition(Duration.seconds(1),sidebar);
        FadeTransition transition1=new FadeTransition(Duration.seconds(2),label);
        transition1.setFromValue(0.0);
        transition1.setToValue(1.0);
        transition.setToX(1600);
        transition.setAutoReverse(true);
        transition.setCycleCount(2);
        transition.play();
        transition1.play();
        transition.setOnFinished(event1 -> {
            try {
                fxml = FXMLLoader.load(getClass().getResource("Exam_setting.fxml"));
                changable_layout.getChildren().removeAll();
                changable_layout.getChildren().setAll(fxml);
            } catch (IOException ex) {

            }
        });
    }

    @FXML
    void MovementMark(ActionEvent event) {
        TranslateTransition transition=new TranslateTransition(Duration.seconds(1),sidebar);
        FadeTransition transition1=new FadeTransition(Duration.seconds(2),label);
        transition1.setFromValue(0.0);
        transition1.setToValue(1.0);
        transition.setToX(1600);
        transition.setAutoReverse(true);
        transition.setCycleCount(2);
        transition.play();
        transition1.play();
        transition.setOnFinished(event1 -> {
            changable_layout.getChildren().removeAll();
            FXMLLoader loader=new FXMLLoader();
            loader.setLocation(getClass().getResource("list_of_students.fxml"));
            try {
                loader.load();
            }catch (IOException e){
                Logger.getLogger(SignUp.class.getName()).log(Level.SEVERE,null,e);
            }
            ListOfStudents listOfStudents=loader.getController();
            listOfStudents.set(teacher);
            Parent xml=loader.getRoot();
            changable_layout.getChildren().setAll(xml);

        });
    }

    @FXML
    void Movementchat(ActionEvent event) {
        TranslateTransition transition=new TranslateTransition(Duration.seconds(1),sidebar);
        FadeTransition transition1=new FadeTransition(Duration.seconds(2),label);
        transition1.setFromValue(0.0);
        transition1.setToValue(1.0);
        transition.setToX(1600);
        transition.setAutoReverse(true);
        transition.setCycleCount(2);
        transition.play();
        transition1.play();
        transition.setOnFinished(event1 -> {
//            try {
//                fxml = FXMLLoader.load(getClass().getResource("Exam_setting.fxml"));
                changable_layout.getChildren().removeAll();
//                changable_layout.getChildren().setAll(fxml);
//            } catch (IOException ex) {
//
//            }
        });
    }

    @FXML
    void movementprofile(ActionEvent event) {
        TranslateTransition transition=new TranslateTransition(Duration.seconds(1),sidebar);
        FadeTransition transition1=new FadeTransition(Duration.seconds(2),label);
        transition1.setFromValue(0.0);
        transition1.setToValue(1.0);
        transition.setToX(1600);
        transition.setAutoReverse(true);
        transition.setCycleCount(2);
        transition.play();
        transition1.play();
        transition.setOnFinished(event1 -> {

        });
    }
    @FXML
public void history(ActionEvent event){
    TranslateTransition transition=new TranslateTransition(Duration.seconds(1),sidebar);
    FadeTransition transition1=new FadeTransition(Duration.seconds(2),label);
    transition1.setFromValue(0.0);
    transition1.setToValue(1.0);
    transition.setToX(1600);
    transition.setAutoReverse(true);
    transition.setCycleCount(2);
    transition.play();
    transition1.play();
    transition.setOnFinished(event1 -> {
        try {
            fxml = FXMLLoader.load(getClass().getResource("history_exam.fxml"));
            changable_layout.getChildren().removeAll();
            changable_layout.getChildren().setAll(fxml);
        } catch (IOException ex) {

        }
    });
}
}
