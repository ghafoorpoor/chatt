package az_far.src.GUI;//package az_far.src.GUI;

import az_far.src.Logic.teacher;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ListOfStudents implements Initializable {
teacher teacher;
    @FXML
    ListView<String> list;
    //ObservableList<String> observableList;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file=new File("student.csv");
        BufferedReader reader=null;
        try {
            reader=new BufferedReader(new FileReader(file));
            //StringBuffer info=null;
            String line;
            while ((line=reader.readLine())!=null){
                list.getItems().add(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
    public void set(teacher t){
    teacher=t;
    }
}
