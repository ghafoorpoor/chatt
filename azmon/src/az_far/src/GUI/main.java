package az_far.src.GUI;

import javafx.animation.FadeTransition;
import javafx.animation.TranslateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class main implements Initializable {


    @FXML VBox vBox;
    Parent fxml;
    int x=0;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        TranslateTransition transition=new TranslateTransition(Duration.seconds(1),vBox);
        transition.setToX(0);
        transition.play();
        transition.setOnFinished((e) ->{
            try{
                fxml = FXMLLoader.load(getClass().getResource("SignUp.fxml"));
                vBox.getChildren().removeAll();
                vBox.getChildren().setAll(fxml);
            }catch(IOException ex){

            }
        });
    }
    @FXML
    private void open_signin(ActionEvent event){
        TranslateTransition t = new TranslateTransition(Duration.seconds(1), vBox);
        FadeTransition f=new FadeTransition(Duration.seconds(1),vBox);
        f.setFromValue(1.0);
        f.setToValue(0.0);
        t.setToX(-300);
        t.play();
        f.play();

        t.setOnFinished((e) ->{
            try{
                FadeTransition fa=new FadeTransition(Duration.seconds(0.2),vBox);
                fa.setFromValue(0.0);
                fa.setToValue(1.0);
                fa.play();
                f.setByValue(1.0);
                fxml = FXMLLoader.load(getClass().getResource("SignIn.fxml"));
                vBox.getChildren().removeAll();
                vBox.getChildren().setAll(fxml);
            }catch(IOException ex){

            }
        });

    }
    @FXML
    private void open_signup(ActionEvent event) {
        //System.out.println("x in signin (second)"+x);
        TranslateTransition t = new TranslateTransition(Duration.seconds(1), vBox);
        FadeTransition f = new FadeTransition(Duration.seconds(1), vBox);
        f.setFromValue(1.0);
        f.setToValue(0.0);
        t.setToX(0);
        t.play();
        f.play();
        t.setOnFinished((e) -> {
            try {
                FadeTransition fa = new FadeTransition(Duration.seconds(0.2), vBox);
                fa.setFromValue(0.0);
                fa.setToValue(1.0);
                fa.play();
                fxml = FXMLLoader.load(getClass().getResource("SignUp.fxml"));
                vBox.getChildren().removeAll();
                vBox.getChildren().setAll(fxml);
            } catch (IOException ex) {

            }
        });
    }
}
