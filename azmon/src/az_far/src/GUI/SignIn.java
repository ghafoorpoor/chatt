package az_far.src.GUI;

import az_far.src.Logic.Entrance;
import az_far.src.Logic.teacher;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SignIn implements Initializable {

    @FXML
    TextField user;
    @FXML
    Button login;
    @FXML
    ComboBox<String> select;
    @FXML TextField pass;
    @FXML
    VBox vBox;
    @FXML
    Label top_content;
    ObservableList<String> list= FXCollections.observableArrayList("student","teacher");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    select.setItems(list);
    }


    public void check_Login(ActionEvent event) {
        Entrance entrance = new Entrance("login", select.getValue());
        entrance.setUser(user.getText());
        entrance.setPassword(pass.getText());
        System.out.println("user:" + entrance.getUser() + "\n" + "pass:" + entrance.getPassword());
        if (select.getValue().equals("student")) {

            try {
                if (entrance.csv()) {
                    top_content.setTextFill(Color.GREEN);
                    top_content.setText("Login succeed!!");

                } else {
                    top_content.setTextFill(Color.RED);
                    top_content.setText("Login failed!!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }else{
            try {
                if (entrance.csv()) {
                    top_content.setTextFill(Color.GREEN);
                    top_content.setText("Login succeed!!");
                    teacher t=new teacher(entrance.getUser(),entrance.getPassword());
                    FXMLLoader loader=new FXMLLoader();
                    loader.setLocation(getClass().getResource("teacher_UI.fxml"));
                    try {
                        loader.load();
                    }catch (IOException e){
                        Logger.getLogger(SignUp.class.getName()).log(Level.SEVERE,null,e);
                    }
                    teacher_UI teacherUI=loader.getController();
                    //teacherUI.set(t);
                    Parent root=loader.getRoot();
                    Stage stage=new Stage();
                    stage.setScene(new Scene(root));
                    stage.showAndWait();

                } else {
                    top_content.setTextFill(Color.RED);
                    top_content.setText("Login failed!!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    }


