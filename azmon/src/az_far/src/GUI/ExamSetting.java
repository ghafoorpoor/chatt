package az_far.src.GUI;

import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTimePicker;
import javafx.animation.RotateTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.shape.Line;
import javafx.stage.FileChooser;
import javafx.util.Duration;

import javax.swing.plaf.FileChooserUI;
import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class ExamSetting implements Initializable {


@FXML
    Button file;
@FXML ListView<String> stu_list1;
@FXML ListView<String> stu_list2;
String exam_folder;
@FXML
    JFXTimePicker timePicker;
@FXML
    JFXDatePicker datePicker;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    File file=new File("student.csv");
    BufferedReader reader=null;
    try {
        reader=new BufferedReader(new FileReader(file));
        String line=reader.readLine();
        while((line=reader.readLine())!=null){
            stu_list1.getItems().add(line);
        }
    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    stu_list1.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }


    public void add_btn(ActionEvent event){
        ObservableList<String> list=stu_list1.getSelectionModel().getSelectedItems();
        StringBuffer id_numbers_exam=new StringBuffer();
        int i=0;
        while (list!=null){
            String[] str=list.get(i).split("\n");
            for(int j=0;j<str.length;j++){
                String[] str2=str[i].split(",");
                stu_list2.getItems().add(str2[4]);
                id_numbers_exam.append(str2[4]+"\n");
            }
            i++;
        }
        File file=new File("C:\\Users\\user\\IdeaProjects\\Exam_System\\src\\Logic\\exams\\"+exam_folder+"\\student.csv");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter writer=null;
        try {
            writer=new PrintWriter(new BufferedWriter(new FileWriter(file)));
            writer.println(id_numbers_exam);
        }catch (IOException e){
        }finally {
            writer.close();
        }
    }


        @FXML
    public void file_add(ActionEvent event){

    }

    public void exam_add(ActionEvent event) {
        FileChooser fc=new FileChooser();
        File file=fc.showOpenDialog(null);
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("txt","*.txt"));
        if(file!=null) {
            file.renameTo(new File("C:\\Users\\user\\IdeaProjects\\Exam_System\\src\\Logic\\exams\\exam.txt"));
            System.out.println("file is transfered!!");
        }else {
            System.out.println("file is not transfered!!");
        }

    }
    public void history_exames(ActionEvent event){

    }
}
