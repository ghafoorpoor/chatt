package az_far.src.GUI;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class HistoryExam implements Initializable {
@FXML
    TextArea textArea;
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        File file=new File("C:\\Users\\user\\IdeaProjects\\Exam_System\\src\\Logic\\exams\\exam.txt");
        StringBuffer info = new StringBuffer();
        BufferedReader reader=null;
        System.out.println("string buffer info =null reached!!");
        try {
            reader=new BufferedReader(new FileReader(file));
            System.out.println("bufferReader reached!!");
            String line;
            while ((line=reader.readLine())!=null){
                info.append(line+"\n");
            }
            System.out.println("while loop reached!!");
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        textArea.setText(info.toString());
    }
}
